function [Cracklist_reduc,intersec_points] = getTotalCrackcoord(Crack_Information)

figure(2)
for i=1:length(Crack_Information)
    plot(Crack_Information(i).Crack.value(:,1),Crack_Information(i).Crack.value(:,2),'b.')
    hold on
end

intersec_points = [];

while(1)
    len1 = length(Crack_Information);
    next = 'Crack';
    %n=numel(fieldnames(Crack_Information));
    %Crack_list = cell(n,1);
    Cracklist_reduc = struct([]);
    
    len = length(Crack_Information);
    k=1;
    
    for i=1:len
        j=1;
        if (~isempty(Crack_Information(i).Crack))
            if ((isfield(Crack_Information(i).Crack,'count'))==0)
                Crack_Information(i).Crack.count = 0;
                count = 0;
            else
                count = Crack_Information(i).Crack.count;
            end
            while(j<=len)
                if(j~=i)
                    first_Crack = Crack_Information(i).Crack.value;
                    CrackWidth = (Crack_Information(i).Crack.CrackWidth)*length(first_Crack);
                    CrackWidth_add = 0;
                    if (~isempty(Crack_Information(j).Crack))
                        next_Crack = Crack_Information(j).Crack.value;
                        [val,~] = intersect(first_Crack,next_Crack,'rows');
                        idx = length(val);
                        if (idx >=1)
                            for m=1:length(val(:,1))
                                intersec_points(k,:) = val(m,:);
                                k=k+1;
                            end
                            if ((min(next_Crack(:,1))>=min(first_Crack(:,1))))
                                Crack_Information(i).Crack.value = joinLineSegment(Crack_Information(i).Crack.value,...,
                                    Crack_Information(j).Crack.value);
                                count = count+1;
                            else
                                Crack_Information(i).Crack.value = joinLineSegment(Crack_Information(j).Crack.value,...,
                                    Crack_Information(i).Crack.value);
                                count = count+1;
                            end
                            Crack_Information(i).Crack.group = [Crack_Information(i).Crack.group;...,
                                Crack_Information(j).Crack.group];
                            Crack_Information(i).Crack.value = unique(Crack_Information(i).Crack.value,'rows','stable');
                            Crack_Information(i).Crack.group = Crack_Information(i).Crack.group(1:length(Crack_Information(i).Crack.value));
                            next_CrackWidth = (Crack_Information(j).Crack.CrackWidth)*length(Crack_Information(j).Crack.value);
                            CrackWidth_add = 1;
                            if(CrackWidth_add)
                                CrackWidth = CrackWidth+next_CrackWidth;
                                %CrackWidth_add = 0;
                            end
                            Crack_Information(j).Crack = [];
                        end
                        
                        if (CrackWidth_add == 0)
                            Crack_total = Crack_Information(i).Crack.value;
                            CrackWidth_avg = CrackWidth/length(Crack_total);
                            Crack_Information(i).Crack.CrackWidth = CrackWidth_avg;
                            if ((max(Crack_total(:,1))-min(Crack_total(:,1))) > (max(Crack_total(:,2))-min(Crack_total(:,2))))
                                max_Cracklen = max(Crack_total(:,1))-min(Crack_total(:,1));
                            else
                                max_Cracklen = max(Crack_total(:,2))-min(Crack_total(:,2));
                            end
                            Crack_Information(i).Crack.CrackLength = max_Cracklen;
                        end
                    end
                end
                j=j+1;
            end
            if (count>0)
                Crack_Information(i).Crack.count = count+1;
            else
                Crack_Information(i).Crack.count = count;
            end
        end
    end
    
    Crack_Information= [Crack_Information.Crack];
    
    for i=1:length(Crack_Information)
        Cracklist_reduc(i).(next) = struct('CrackID',Crack_Information(i).CrackID,...,
            'CrackLength',Crack_Information(i).CrackLength,...,
            'CrackWidth',Crack_Information(i).CrackWidth,...,
            'value',Crack_Information(i).value,'group',Crack_Information(i).group,...,
            'count',Crack_Information(i).count);
    end
    
    len2 = length(Cracklist_reduc);
    Crack_Information = Cracklist_reduc;
    if ((len2-len1)==0)
        break;
    end
end
end


function C = joinLineSegment(A,B)

if (size(B,1) > size(A,1))
    dir = 0;
else
    dir = 1;
end

if (dir==0)
    for i=1:size(A,1)
        [~,indx] = min(abs(B(:,1)-A(i,1)));
        B=[B(1:indx,:);A(i,:);B((indx+1:end),:)];
        %break;
    end
    C = B;
else
    for i=1:size(B,1)
        [~,indx] = min(abs(A(:,1)-B(i,1)));
        A=[A(1:indx,:);B(i,:);A((indx+1:end),:)];
        %break;
    end
    C =A;
end
end