function Crack_class = parseXML(filename)
% PARSEXML Convert XML file to a MATLAB structure.
try
   tree = xmlread(filename);
catch
   error('Failed to read XML file %s.',filename);
end

% Recurse over child nodes.
try
    mlStruct = parseChildNodes(tree);
    for m=1:length(mlStruct.Children)
        if (strcmp(mlStruct.Children(m).Name,'CrackInformation')==1)
            break;
        end
    end
    
    for n=1:length(mlStruct.Children(m).Children)
       if(strcmp(mlStruct.Children(m).Children(n).Name,'CrackList')==1)
           break;
       end
    end
    CrackInfo=mlStruct.Children(m).Children(n);
    Cracklist = cell(round(length(CrackInfo.Children)/2)-1,1);
    y=1;
    
    for j=1:length(CrackInfo.Children)
        if (rem(j,2)==0)
            CrackNode=CrackInfo.Children(j).Children;
            Cracklist{y}= getCrackNodeInformation(CrackNode);
            y=y+1;
        end
    end
    
    Crack_Index = getCrackcoord(Cracklist);
    Crack_class = getCrackClassification(Crack_Index);
    
catch
   error('Unable to parse XML file %s.',filename);
end


% ----- Local function PARSECHILDNODES -----
function children = parseChildNodes(theNode)
% Recurse over node children.
children = [];
if theNode.hasChildNodes
   childNodes = theNode.getChildNodes;
   numChildNodes = childNodes.getLength;
   allocCell = cell(1, numChildNodes);

   children = struct('Name', allocCell, 'Attributes', allocCell,'Data', allocCell, 'Children', allocCell);

   %if(numChildNodes>=1)
    for count = 1:numChildNodes
        theChild = childNodes.item(count-1);
        children(count) = makeStructFromNode(theChild);
    end
   %end
end

% ----- Local function MAKESTRUCTFROMNODE -----
function nodeStruct = makeStructFromNode(theNode)
% Create structure of node info.

nodeStruct = struct('Name', char(theNode.getNodeName),'Attributes', parseAttributes(theNode),'Data', '','Children', parseChildNodes(theNode));

if any(strcmp(methods(theNode), 'getData'))
   nodeStruct.Data = char(theNode.getData); 
else
   nodeStruct.Data = '';
end

% ----- Local function PARSEATTRIBUTES -----
function attributes = parseAttributes(theNode)
% Create attributes structure.

attributes = [];
if theNode.hasAttributes
   theAttributes = theNode.getAttributes;
   numAttributes = theAttributes.getLength;
   allocCell = cell(1, numAttributes);
   attributes = struct('Name', allocCell, 'Value', allocCell);

   for count = 1:numAttributes
      attrib = theAttributes.item(count-1);
      attributes(count).Name = char(attrib.getName);
      attributes(count).Value = char(attrib.getValue);
   end
end

% ----- Local function GETCRACKNODEINFORMATION -----
function CrackInformation = getCrackNodeInformation(CrackNode)
y=1;
X=zeros(round(length(CrackNode)/2),1);
Y=zeros(round(length(CrackNode)/2),1);

for i=1:length(CrackNode)
    if(strcmp(CrackNode(i).Name,'Node')==1)
         Node=CrackNode(i).Children;
         X(y)=str2double(Node(2).Children.Data)./1000;
         Y(y)=str2double(Node(4).Children.Data)./1000;
         y=y+1;
    end
end

X=X(X~=0);
Y=Y(Y~=0);
CrackInformation=[X,Y];

function Crack_List = getCrackcoord(CrackInformation)
numCracks=length(CrackInformation);

for i=1:numCracks
    my_field = strcat('Crack',num2str(i));
    Crack_List.(my_field) = cell2mat(CrackInformation(i));
end

function Crack_class = getCrackClassification(CrackInformation)

numCracks = numel(fieldnames(CrackInformation));
Crack_class = cell(numCracks,1);
figure(1)

for i=1:numCracks
    my_field = strcat('Crack',num2str(i));
    x=CrackInformation.(my_field)(:,1);
    y=CrackInformation.(my_field)(:,2);
    hold on
    plot(x,y);
    A = [x ones(size(x))];
    y_eq=(A'*A)\(A'*y);
    hold on
    %plot(x,y_eq(1)*x+y_eq(2),'r');
    if(y_eq(1) < 1)
        Crack_class{i} = 'Transverse';
    end
    
    if (y_eq(1) > 1)
        Crack_class{i} = 'Longitudinal';
    end
end

