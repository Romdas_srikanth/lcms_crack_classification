function isSquare = isSquare(pointList)

isSquare = 0;

sides = zeros(3,1);
sides(1) = distCalc(pointList(1,:),pointList(2,:));
sides(2) = distCalc(pointList(1,:),pointList(3,:));
sides(3) = distCalc(pointList(1,:),pointList(4,:));

equalSide1 = -1;
%equalSide2 = -1;
unequalSide = -1;

if (sides(1) == sides(2))
    if (sides(1) ~= sides(3))
        equalSide1 = 1;
        %equalSide2 = 2;
        unequalSide = 3;
    end
elseif (sides(2) == sides(3))
    if (sides(2) ~= sides(1))
        equalSide1 = 2;
        %equalSide2 = 3;
        unequalSide = 1;
    end
elseif (sides(1) == sides(3))
    if (sides(1) ~= sides(2))
        equalSide1 =1;
        %equalSide2 = 3;
        unequalSide = 2;
    end
else
    isSquare = isRectangle(pointList);
end

if (equalSide1 ~= -1)
   opposing = 0;
   switch (unequalSide)
       case 1
           opposing = distCalc(pointList(3,:),pointList(4,:));
       case 2
           opposing = distCalc(pointList(2,:),pointList(4,:));
       case 3
           opposing = distCalc(pointList(2,:),pointList(3,:));
   end     
end

if (opposing == sides(unequalSide,:))
    diagonal = opposing;
    adjacent = sides(equalSide1,:);
    for a=1:4
        diagonalCount = 0;
        adjacentCount = 0;
        for b=1:4
            if (a~=b)
                dist = distCalc(pointList(a,:),pointList(b,:));
                if (dist == diagonal)
                    diagonalCount = diagonalCount+1;
                elseif (dist == adjacent)
                    adjacentCount = adjacentCount+1;
                end
            end
        end
    end
    
    if ((diagonal == 1) && (adjacent == 2))
        isSquare = 1;
        %resCoord = pointList;
    end
    
end
end


function resCoord = isRectangle(pointList)

cx = mean(pointList(:,1));
cy = mean(pointList(:,2));

dd1 = sqrt((cx-pointList(1,1))^2 + (cy-pointList(1,2))^2);
dd2 = sqrt((cx-pointList(2,1))^2 + (cy-pointList(2,2))^2);
dd3 = sqrt((cx-pointList(3,1))^2 + (cy-pointList(3,2))^2);
dd4 = sqrt((cx-pointList(4,1))^2 + (cy-pointList(4,2))^2);

if ((dd1 == dd2) && (dd2 == dd3) && (dd3 == dd4))
    resCoord = pointList;
end

end

function dist = distCalc(point1,point2)

dist = (point1(1,1)-point2(1,1))^2 + (point1(1,2)-point2(1,2))^2;
dist = sqrt(dist);

end