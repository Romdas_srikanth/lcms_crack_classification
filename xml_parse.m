function CrackClass = xml_parse(s)

close all;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%This bit can be done through C++, use fread to
%create a character array and pass that to the dll as input
%Input to the dll can then be defined as a char or uint8 array of 1 x Inf size.
fid = fopen(s,'r');
F = fread(fid);
str = char(F');
fclose(fid);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
z = parse_xml(str);

len = length(z.children(2).children);
param = z.children(2).children;

for m=1:len
    if (strcmp(param(m).tag,'CrackInformation')==1)
        break;
    end
end

for n=1:length(param(m).children)
    if(strcmp(param(m).children(n).tag,'CrackList')==1)
        break;
    end
end

% for p=1:len
%     if(strcmp(param(p).tag,'CrackClassification')==1)
%         break;
%     end
% end

Cracklist=param(m).children(n).children;

Crack_list = struct([]);
next = 'Crack';

for i=1:length(Cracklist)
    %next = strcat('Crack',num2str(i));
    CrackNode = Cracklist(i).children;
    CrackInfo=getCrackNodeInformation(CrackNode);
    %new_tag = strcat('Crack',num2str(i));
    
    for j=1:length(CrackNode)
        if(strcmp(CrackNode(j).tag,'CrackID')==1)
            break;
        end
    end
    
    Crack_list(i).(next) = struct('CrackID',CrackNode(j).value,'CrackLength',...,
        str2num(CrackNode(j+1).value)*1000,...,
        'CrackWidth',str2num(CrackNode(j+3).value),...,
        'value',CrackInfo(:,(1:2)),'group',CrackInfo(:,3));
end

% if (~isempty(Cracklist))
%     CrackClassification = param(p).children;
% end

figure(1)
hold on

for i=1:length(Crack_list)
    plot(Crack_list(i).Crack.value(:,1),Crack_list(i).Crack.value(:,2),...,
        '--mo','MarkerEdgeColor','k','MarkerFaceColor',[1 0 1],'MarkerSize',5);
    hold on
end
hold off

Crack_list = seperateCracks(Crack_list);

[Crack_list,AlligCrack_list] = seperatePaveClass(Crack_list);

Crack_list = sortArrayAscending(Crack_list);

if(~isempty(AlligCrack_list))
    AlligCrack_list = sortArrayAscending(AlligCrack_list);
end

Crack_list = getTotalCrackcoord(Crack_list);

if (~isempty(AlligCrack_list))
    AlligCrack_list = getTotalCrackcoord(AlligCrack_list);
end

%figure(1)
hold on
xlim([0 4000])
ylim([0 10000])
%sz = 40;
legendInfo = cell(length(Crack_list),1);

for i=1:length(Crack_list)
    legendInfo{i} = ['CrackID' Crack_list(i).Crack.CrackID];
    plot(Crack_list(i).Crack.value(:,1),Crack_list(i).Crack.value(:,2),'mo--',...
        'MarkerEdgeColor','k','MarkerFaceColor',rand(1,3),'MarkerSize',3);
    hold on
    % %scatter(Crack_list(i).Crack.value(:,1),Crack_list(i).Crack.value(:,2),'MarkerEdgeColor',[0 .5 .5],...
    %               'MarkerFaceColor',[0 .7 .7],...
    %               'LineWidth',1.5)
    
end

hold off
legend (legendInfo);

counter = 1;
Crack_filt = addCrack(Crack_list,counter);

if (~isempty(AlligCrack_list))
    AlligCrack_filt = addCrack(AlligCrack_list,counter);
else
    AlligCrack_filt = struct([]);
end

while(1)
    counter = 0;
    len1 = length(Crack_filt);
    Crack_filt = addCrack(Crack_filt,counter);
    len2 = length(Crack_filt);
    
    if ((len2-len1)==0)
        break;
    end
end

CrackClass_full = setCrackClass(Crack_filt);

if (~isempty(AlligCrack_list))
    AlligClass_full = setAlligCrackClass(AlligCrack_filt);
    
    for i=1:length(AlligClass_full)
        CrackClass_full(end+1)=AlligClass_full(i);
        Crack_filt(end+1)=AlligCrack_filt(i);
    end
end

[CrackClass,Crack_filt] = AddFatigueCracks(Crack_filt,CrackClass_full);

[CrackClass,Crack_filt] = AddTransverseCracks(Crack_filt,CrackClass);

[CrackClass,~] = AddLongitudinalCracks(Crack_filt,CrackClass);

end

%%%%%%%%%%%%%%%%%%%%%%%%%Check if array is decreasing or increasing%%%%%%%%
function CrackInfo = sortArrayAscending(CrackInfo)

for i=1:length(CrackInfo)
    if (~isempty(CrackInfo(i).Crack.value))
        if(length(CrackInfo(i).Crack.value(:,1))>1)
            xval = CrackInfo(i).Crack.value(:,1);
            %yval = CrackInfo(i).value(:,2);
            if(length(xval)>=4)
                if ((xval(1) >= xval(2)) && (xval(end-2)>= xval(end-1)))
                    CrackInfo(i).Crack.value = flipud(CrackInfo(i).Crack.value);
                elseif (length(xval)>=8)
                    if ((xval(3) >= xval(4)) && (xval(end-4)>= xval(end-3)))
                        CrackInfo(i).Crack.value = flipud(CrackInfo(i).Crack.value);
                    end
                end
            else
                if ((xval(1) >= xval(2)))
                    CrackInfo(i).Crack.value = flipud(CrackInfo(i).Crack.value);
                end
            end
        end
    end
end

end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function Crack_list = seperateCracks(Crack_list)
i=1;
len = length (Crack_list);

while(i<=len)
    t=Crack_list(i).Crack.group;
    x_len = Crack_list(i).Crack.value(:,1);
    y_len = Crack_list(i).Crack.value(:,2);
    
    if (length(y_len) > length(x_len))
        Crack_len = max(y_len)-min(y_len);
    else
        Crack_len = max(x_len)-min(x_len);
    end
    
    %if (Crack_len >300)
    [~,ia]= unique(t);
    c = 1;
    [x,y]=ismember(c,ia);
    
    if(length(Crack_list(i).Crack.group)>=2)
        if (y==1)
            Crack_list(i).Crack.group(1) = Crack_list(i).Crack.group(2);
        else
            Crack_list(i).Crack.group(end) = Crack_list(i).Crack.group(end-1);
        end
    end
    ia(y(x))=[];
    %while(length(C)>1)
    if (length(ia)>=1)
        j=1;
        %crackgrp = Crack_list(i).Crack.group;
        while(~isempty(ia))
            crackval = Crack_list(i).Crack.value;
            [~,ia]= unique(Crack_list(i).Crack.group);
            c = 1;
            [x,y]=ismember(c,ia);
            ia(y(x))=[];
            
            if (isempty(ia))
                break;
            end
            
            crackval_new = crackval((ia(j):end),:);
            crackval_first = crackval((1:ia(j)-1),:);
            
            x_len_first = max(crackval_first(:,1)) - min(crackval_first(:,1));
            y_len_first = max(crackval_first(:,2)) - min(crackval_first(:,2));
            
            if(y_len_first > x_len_first)
                crack_len_first = y_len_first;
            else
                crack_len_first = x_len_first;
            end
            
            %crackval_new = crackval((ia(j):end),:);
            x_len_new = max(crackval_new(:,1)) - min(crackval_new(:,1));
            y_len_new = max(crackval_new(:,2)) - min(crackval_new(:,2));
            
            if(y_len_new > x_len_new)
                crack_len_new = y_len_new;
            else
                crack_len_new = x_len_new;
            end
            
            crackgrp_new = Crack_list(i).Crack.group((ia(j):end),:);
            crackgrp_first = Crack_list(i).Crack.group((1:ia(j)-1),:);
            
            Crack_list(i).('Crack') = struct('CrackID',Crack_list(i).Crack.CrackID,'CrackLength',...,
                crack_len_first,'CrackWidth',Crack_list(i).Crack.CrackWidth,...,
                'value',crackval_first,'group',crackgrp_first);
            
            Crack_list(end+1).('Crack') = struct('CrackID',Crack_list(i).Crack.CrackID,'CrackLength',...,
                crack_len_new,'CrackWidth',Crack_list(i).Crack.CrackWidth,...,
                'value',crackval_new,'group',crackgrp_new);
            
        end
    else
        Crack_list(i).('Crack') = struct('CrackID',Crack_list(i).Crack.CrackID,'CrackLength',...,
            Crack_list(i).Crack.CrackLength,'CrackWidth',Crack_list(i).Crack.CrackWidth,...,
            'value',Crack_list(i).Crack.value,'group',Crack_list(i).Crack.group);
        
    end
    
    i=i+1;
    len = length(Crack_list);
end

end

%%%%%%%%%%%%%%%%% Get XY co-ordinates for Classification%%%%%%%%%%%%%%%%%%%
function Crack_coord = getCrackNodeInformation(CrackNode)
for j=1:length(CrackNode)
    if (strcmp(CrackNode(j).tag,'Node')==1)
        break;
    end
end

X = zeros(length(CrackNode)-j+1,1);
Y = zeros(length(CrackNode)-j+1,1);
group = zeros(length(CrackNode)-j+1,1);
y=1;

for k=j:length(CrackNode)
    if(strcmp(CrackNode(k).tag,'Node')==1)
        Node=CrackNode(k).children;
        X(y) = str2num(Node(1).value);
        Y(y) = str2num(Node(2).value);
        group(y) = str2num(Node(5).value);
        y=y+1;
    end
end

%X=X(X~=0);
%Y=Y(Y~=0);
Crack_coord=[X,Y,group];
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%Add adjacent cracks within 0.3m%%%%%%%%%%%%%%%%%%%%%%%%%%
function Cracklist_filt = addCrack(Crack_Information,counter)

next = 'Crack';
Cracklist_filt = struct([]);
len = length(Crack_Information);
add_Crack = 0;
Crack_Inf = Crack_Information;

for i=1:len
    j=1;
    if (~isempty(Crack_Information(i).Crack))
        while(j<=len)
            first_Crack = Crack_Inf(i).Crack.value;
            if(j~=i)
                firstCrack_width = Crack_Information(i).Crack.CrackWidth;
                if (~isempty(Crack_Information(j).Crack))
                    next_Crack = Crack_Information(j).Crack.value;
                    nextCrack_width = Crack_Information(j).Crack.CrackWidth;
                    min_dist = distCalc(first_Crack,next_Crack);
                    if (min_dist < 300)
                        add_Crack = 1;
                        if ((max(next_Crack(:,1)-min(next_Crack(:,1)))) >= (max(next_Crack(:,2)-min(next_Crack(:,2)))))
                            cracklen = max(next_Crack(:,1)-min(next_Crack(:,1)));
                           % cracklen2 =2000;
                        else
                            cracklen = max(next_Crack(:,2)-min(next_Crack(:,2)));
                            %cracklen1 = 400;
                        end
                        if ((cracklen < 300))
%                             if ((Crack_Information(j).Crack.count == 0) || (Crack_Information(i).Crack.count == 0))
                                Crack_Information(i).Crack.count = Crack_Information(i).Crack.count+Crack_Information(j).Crack.count+1;
%                             else
%                                 Crack_Information(i).Crack.count = Crack_Information(i).Crack.count+Crack_Information(j).Crack.count;
%                             end
                            if ((min(next_Crack(:,1))>=min(first_Crack(:,1))))
                                first_Crack = Crack_Inf(i).Crack.value;
                                if (length(next_Crack(:,1))>=2)
                                    Crack_Information(i).Crack.value = [Crack_Information(i).Crack.value;...,
                                        Crack_Information(j).Crack.value];
                                else
                                    Crack_Information(i).Crack.value = joinLineSegment(Crack_Information(i).Crack.value,...,
                                        Crack_Information(j).Crack.value);
                                end
                                Crack_Information(i).Crack.group = [Crack_Information(i).Crack.group;...,
                                    Crack_Information(j).Crack.group];
                                %Crack_Inf(i).Crack.value = Crack_Information(i).Crack.value;
                            else
                                first_Crack = Crack_Inf(i).Crack.value;
                                if (length(first_Crack(:,1))>=2)
                                    Crack_Information(i).Crack.value = [Crack_Information(j).Crack.value;...,
                                        Crack_Information(i).Crack.value];
                                else
                                    Crack_Information(i).Crack.value = joinLineSegment(Crack_Information(j).Crack.value,...,
                                        Crack_Information(i).Crack.value);
                                end
                                Crack_Information(i).Crack.group = [Crack_Information(j).Crack.group;...,
                                    Crack_Information(i).Crack.group];
                                %Crack_Inf(i).Crack.value = Crack_Information(i).Crack.value;
                            end
                            
                            
                            Crack_val = Crack_Information(i).Crack.value;
                            if (counter==1)
%                                 if (length(first_Crack(:,1)) >= length(next_Crack(:,1)))
                                    Crack_Information(i).Crack.primarydist = first_Crack;
                                    Crack_Inf(i).Crack.value = first_Crack;
%                                 else
%                                     Crack_Information(i).Crack.primarydist = next_Crack;
%                                     Crack_Inf(i).Crack.value = next_Crack;
%                                 end
                            end
                            
                            firstCracklen = length(first_Crack(:,1));
                            nextCracklen = length(next_Crack(:,1));
                            
                            Crack_Information(i).Crack.CrackWidth = ((firstCrack_width*firstCracklen)+(nextCrack_width*nextCracklen))/(firstCracklen+nextCracklen);
%                             if (firstCrack_width > nextCrack_width)
%                                 Crack_Information(i).Crack.CrackWidth = firstCrack_width;
%                             else
%                                 Crack_Information(i).Crack.CrackWidth = nextCrack_width;
%                             end
                            if (length(max(Crack_val(:,1)-min(Crack_val(:,1)))) >= length(max(Crack_val(:,2)-min(Crack_val(:,2)))))
                                Crack_Information(i).Crack.CrackLength = max(Crack_val(:,1)-min(Crack_val(:,1)));
                            else
                                Crack_Information(i).Crack.CrackLength = max(Crack_val(:,2)-min(Crack_val(:,2)));
                            end
                            Crack_Information(j).Crack = [];
                        end
                        %                     else
                        %                         if (counter==1)
                        %                             Crack_Information(i).Crack.primarydist = Crack_Information(i).Crack.value;
                        %                         end
                    end
                end
            end
            j=j+1;
        end
        
    end
    
    if ((add_Crack == 0)&&(counter==1))
        if (~isempty(Crack_Information(i).Crack))
            Crack_Information(i).Crack.primarydist = Crack_Information(i).Crack.value;
        end
    end
    
    add_Crack = 0;
    
    if (~isempty(Crack_Information(i).Crack))
        if ((isfield(Crack_Information(i).Crack,'primarydist'))==0)
            Crack_Information(i).Crack.primarydist = Crack_Information(i).Crack.value;
        end
    end
    
end

Crack_Information = [Crack_Information.Crack];

for i=1:length(Crack_Information)
    Cracklist_filt(i).(next) = struct('CrackID',Crack_Information(i).CrackID,...,
        'CrackLength',Crack_Information(i).CrackLength,...,
        'CrackWidth',Crack_Information(i).CrackWidth,...,
        'value',Crack_Information(i).value,'group',Crack_Information(i).group,...,
        'count',Crack_Information(i).count,'primarydist',Crack_Information(i).primarydist);
end

end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%Add crack-coordinates together if they have same
%%%%%%%%%%%%%%%%vertices%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function Cracklist_reduc = getTotalCrackcoord(Crack_Information)

while(1)
    len1 = length(Crack_Information);
    next = 'Crack';
    %n=numel(fieldnames(Crack_Information));
    %Crack_list = cell(n,1);
    Cracklist_reduc = struct([]);
    len = length(Crack_Information);
    
    for i=1:len
        j=1;
        if (~isempty(Crack_Information(i).Crack))
            if ((isfield(Crack_Information(i).Crack,'count'))==0)
                Crack_Information(i).Crack.count = 0;
                count = 0;
            else
                count = Crack_Information(i).Crack.count;
            end
            while(j<=len)
                if(j~=i)
                    first_Crack = Crack_Information(i).Crack.value;
                    CrackWidth = (Crack_Information(i).Crack.CrackWidth)*length(first_Crack);
                    CrackWidth_add = 0;
                    if (~isempty(Crack_Information(j).Crack))
                        next_Crack = Crack_Information(j).Crack.value;
                        [val,~] = intersect(first_Crack,next_Crack,'rows');
                        idx = length(val);
                        if (idx >=1)
                            if ((min(next_Crack(:,1))>=min(first_Crack(:,1))))
                                Crack_Information(i).Crack.value = joinLineSegment(Crack_Information(i).Crack.value,...,
                                    Crack_Information(j).Crack.value);
                                count = count+1;
                            else
                                Crack_Information(i).Crack.value = joinLineSegment(Crack_Information(j).Crack.value,...,
                                    Crack_Information(i).Crack.value);
                                count = count+1;
                            end
                            Crack_Information(i).Crack.group = [Crack_Information(i).Crack.group;...,
                                Crack_Information(j).Crack.group];
                            Crack_Information(i).Crack.value = unique(Crack_Information(i).Crack.value,'rows','stable');
                            Crack_Information(i).Crack.group = Crack_Information(i).Crack.group(1:length(Crack_Information(i).Crack.value));
                            next_CrackWidth = (Crack_Information(j).Crack.CrackWidth)*length(Crack_Information(j).Crack.value);
                            CrackWidth_add = 1;
                            if(CrackWidth_add)
                                CrackWidth = CrackWidth+next_CrackWidth;
                                %CrackWidth_add = 0;
                            end
                            Crack_Information(j).Crack = [];
                        end
                        
                        if (CrackWidth_add == 0)
                            Crack_total = Crack_Information(i).Crack.value;
                            CrackWidth_avg = CrackWidth/length(Crack_total);
                            Crack_Information(i).Crack.CrackWidth = CrackWidth_avg;
                            if ((max(Crack_total(:,1))-min(Crack_total(:,1))) > (max(Crack_total(:,2))-min(Crack_total(:,2))))
                                max_Cracklen = max(Crack_total(:,1))-min(Crack_total(:,1));
                            else
                                max_Cracklen = max(Crack_total(:,2))-min(Crack_total(:,2));
                            end
                            Crack_Information(i).Crack.CrackLength = max_Cracklen;
                        end
                    end
                end
                j=j+1;
            end
            if (count>0)
                Crack_Information(i).Crack.count = count+1;
            else
                Crack_Information(i).Crack.count = count;
            end
        end
    end
    
    Crack_Information= [Crack_Information.Crack];
    
    for i=1:length(Crack_Information)
        Cracklist_reduc(i).(next) = struct('CrackID',Crack_Information(i).CrackID,...,
            'CrackLength',Crack_Information(i).CrackLength,...,
            'CrackWidth',Crack_Information(i).CrackWidth,...,
            'value',Crack_Information(i).value,'group',Crack_Information(i).group,...,
            'count',Crack_Information(i).count);
    end
    
    len2 = length(Cracklist_reduc);
    Crack_Information = Cracklist_reduc;
    if ((len2-len1)==0)
        break;
    end
end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Remove the already classified co-ordinates from the co-ordinate list and
%re-classify the rest. (Only for alligator Cracks).
function [CrackClass_red,AlligCrack_list] = seperatePaveClass(Cracklist)

next = 'Crack';
CrackClass_red =struct([]);
AlligCrack_list = struct([]);

Allig_Crack = Cracklist;

for i=1:length(Cracklist)
    if(~isempty(Cracklist(i).Crack))
        grp_num = unique(Cracklist(i).Crack.group);
        if ((length(grp_num)==1) && (grp_num==2))
            Cracklist(i).Crack=[];
        else
            if (length(grp_num)>1)
                if (grp_num(grp_num == 2))
                    Cracklist(i).Crack=[];
                else
                    Allig_Crack(i).Crack=[];
                end
            else
                Allig_Crack(i).Crack=[];
            end
        end
        
    end
end
%Cracklist.Crack=[Cracklist.Crack];


Cracklist = [Cracklist.Crack];
Allig_Crack = [Allig_Crack.Crack];

for i=1:length(Cracklist)
    CrackClass_red(i).(next) = struct('CrackID',Cracklist(i).CrackID,...,
        'CrackLength',Cracklist(i).CrackLength,...,
        'CrackWidth',Cracklist(i).CrackWidth,...,
        'value',Cracklist(i).value,'group',Cracklist(i).group);
end

for i=1:length(Allig_Crack)
    AlligCrack_list(i).(next) = struct('CrackID',Allig_Crack(i).CrackID,...,
        'CrackLength',Allig_Crack(i).CrackLength,...,
        'CrackWidth',Allig_Crack(i).CrackWidth,...,
        'value',Allig_Crack(i).value,'group',Allig_Crack(i).group);
end

end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function AlligCrack_struct = setAlligCrackClass(AlligCrackNode)

AlligCrack_struct = struct([]);
next = 'Crack';

for i=1:length(AlligCrackNode)
    if (AlligCrackNode(i).Crack.CrackWidth <= 6)
        Crack_Severity = 'Low';
    end
    
    if (AlligCrackNode(i).Crack.CrackWidth > 6 && AlligCrackNode(i).Crack.CrackWidth <= 19)
        Crack_Severity = 'Moderate';
    end
    
    if (AlligCrackNode(i).Crack.CrackWidth > 19)
        Crack_Severity = 'High';
    end
    
    min_x = min(AlligCrackNode(i).Crack.value(:,1));
    min_y = min(AlligCrackNode(i).Crack.value(:,2));
    max_x = max(AlligCrackNode(i).Crack.value(:,1));
    max_y = max(AlligCrackNode(i).Crack.value(:,2));
    
    %rectangle('Position',[min_x,min_y,(max_x-min_x),(max_y-min_y)],'EdgeColor','m','LineWidth',1.5);
    Crack_area = ((max_x-min_x)/1000)*((max_y-min_y)/1000);
    
    AlligCrack_struct(i).(next) =  struct('CrackID',AlligCrackNode(i).Crack.CrackID,'CrackLength',...,
        AlligCrackNode(i).Crack.CrackLength,...,
        'CrackWidth',AlligCrackNode(i).Crack.CrackWidth,...,
        'CrackArea',Crack_area,'Classif','Fatigue','Severity',...,
        Crack_Severity,'BoundingBox',[min_x,min_y;max_x,max_y]);
    
end


end


%%%%%%%%%%%%%%%%%%Initial Crack Classification%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function Crackstruct = setCrackClass(CrackNode)

%Transverse Crack Classification
Crackstruct = struct([]);
next = 'Crack';

for i=1:length(CrackNode)
    
    if (CrackNode(i).Crack.CrackWidth <= 6)
        Crack_Severity = 'Low';
    end
    
    if (CrackNode(i).Crack.CrackWidth > 6 && CrackNode(i).Crack.CrackWidth <= 19)
        Crack_Severity = 'Moderate';
    end
    
    if (CrackNode(i).Crack.CrackWidth > 19)
        Crack_Severity = 'High';
    end
    
    x = CrackNode(i).Crack.primarydist(:,1);
    y = CrackNode(i).Crack.primarydist(:,2);
    A = [x ones(size(x))];
    y_eq = (A'*A)\(A'*y);
    
    primarydist = CrackNode(i).Crack.primarydist;
    
    if (abs(y_eq(1)) <= sqrt(3)) %30 degree slope, can be adjusted
        p_x = primarydist(:,1);
        primarydist_x = max(primarydist(:,1))-min(primarydist(:,1));
        
        if (primarydist_x > 300)
            if((x(end)< max(p_x)) ||  (x(1)> min(p_x)))
                max_px = max(p_x)-10;
                min_px = min(p_x)+10;
                %                 M = sort(p_x);
                %                 min_p2x = M(2);
                %                 max_p2x = M(end-1);
            else
                max_px = max(p_x);
                min_px = min(p_x);
                %                 M = sort(p_x);
                %                 min_p2x = M(2);
                %                 max_p2x = M(end-1);
            end
        else
            max_px = max(p_x);
            min_px = min(p_x);
        end
        
        if (primarydist_x >= 300)
            min_x = min(CrackNode(i).Crack.value(:,1));
            min_y = min(CrackNode(i).Crack.value(:,2));
            max_x = max(CrackNode(i).Crack.value(:,1));
            max_y = max(CrackNode(i).Crack.value(:,2));
            
            if((x(end)>= max_px && x(1)<=min_px) || ((x(end)<=min_px) && x(1)>=max_px))
                %if((x(end-1)>= max_p2x && x(2)<=min_p2x) || ((x(end-1)<=min_p2x) && x(2)>=max_p2x))
                rectangle('Position',[min_x,min_y,(max_x-min_x),(max_y-min_y)],'EdgeColor','b','LineWidth',1.5);
                Crackstruct(i).(next) = struct('CrackID',CrackNode(i).Crack.CrackID,'CrackLength',...,
                    (max_x-min_x),'CrackWidth',CrackNode(i).Crack.CrackWidth,...,
                    'CrackArea',0,'Classif','Transverse','Severity',Crack_Severity,...,
                    'BoundingBox',[min_x,min_y;max_x,max_y]);
            else
                %rectangle('Position',[min_x,min_y,(max_x-min_x),(max_y-min_y)],'EdgeColor','r','LineWidth',1.5);
                Crack_area = ((max_x-min_x)/1000)*((max_y-min_y)/1000);
                Crackstruct(i).(next) =  struct('CrackID',CrackNode(i).Crack.CrackID,'CrackLength',...,
                    CrackNode(i).Crack.CrackLength,...,
                    'CrackWidth',CrackNode(i).Crack.CrackWidth,...,
                    'CrackArea',Crack_area,'Classif','Fatigue','Severity',Crack_Severity,'BoundingBox',[min_x,min_y;max_x,max_y]);
                
            end
        else
            min_x = min(CrackNode(i).Crack.value(:,1));
            min_y = min(CrackNode(i).Crack.value(:,2));
            max_x = max(CrackNode(i).Crack.value(:,1));
            max_y = max(CrackNode(i).Crack.value(:,2));
            %rectangle('Position',[min_x,min_y,(max_x-min_x),(max_y-min_y)],'EdgeColor','r','LineWidth',1.5);
            Crack_area = ((max_x-min_x)/1000)*((max_y-min_y)/1000);
            Crackstruct(i).(next) =  struct('CrackID',CrackNode(i).Crack.CrackID,'CrackLength',...,
                CrackNode(i).Crack.CrackLength,...,
                'CrackWidth',CrackNode(i).Crack.CrackWidth,...,
                'CrackArea',Crack_area,'Classif','Fatigue','Severity',Crack_Severity,'BoundingBox',[min_x,min_y;max_x,max_y]);
        end
    else
        if (abs(y_eq(1)) > sqrt(3))
            %p_y = primarydist(:,2);
            %             max_py = max(p_y);
            %             min_py = min(p_y);
            primarydist = CrackNode(i).Crack.primarydist;
            primarydist_y = max(primarydist(:,2))-min(primarydist(:,2));
            
            %             if (primarydist_y > 1000)
            %                 if((y(end)< max(p_y)) ||  (y(1)> min(p_y)))
            %                     max_py = max(p_y)-0;
            %                     min_py = min(p_y)+0;
            % %                     M = sort(p_y);
            % %                     min_p2y = M(2);
            % %                     max_p2y = M(end-1);
            %                 else
            %                     max_py = max(p_y);
            %                     min_py = min(p_y);
            % %                     M = sort(p_y);
            % %                     min_p2y = M(2);
            % %                     max_p2y = M(end-1);
            %                 end
            %             else
            %                 max_py = max(p_y);
            %                 min_py = min(p_y);
            %             end
            %
            if(primarydist_y>1000)
                min_x = min(CrackNode(i).Crack.value(:,1));
                min_y = min(CrackNode(i).Crack.value(:,2));
                max_x = max(CrackNode(i).Crack.value(:,1));
                max_y = max(CrackNode(i).Crack.value(:,2));
                %if((y(end)>= max_py && y(1)<=min_py) || ((y(end)<=min_py) && y(1)>=max_py))
                %                     if((y(end-1)>= max_p2y && y(2)<=min_p2y) || ((y(end-1)<=min_p2y) && y(2)>=max_p2y))
                rectangle('Position',[min_x,min_y,(max_x-min_x),(max_y-min_y)],'EdgeColor','k','LineWidth',1.5);
                Crackstruct(i).(next) = struct('CrackID',CrackNode(i).Crack.CrackID,'CrackLength',...,
                    max_y-min_y,'CrackWidth',CrackNode(i).Crack.CrackWidth,...,
                    'CrackArea',0,'Classif','Longitudinal','Severity',Crack_Severity,...,
                    'BoundingBox',[min_x,min_y;max_x,max_y]);               
            else
                if ((primarydist_y >300) && (CrackNode(i).Crack.count==0))
                    min_x = min(CrackNode(i).Crack.value(:,1));
                    min_y = min(CrackNode(i).Crack.value(:,2));
                    max_x = max(CrackNode(i).Crack.value(:,1));
                    max_y = max(CrackNode(i).Crack.value(:,2));
                    %if((y(end)>= max_py && y(1)<=min_py) || ((y(end)<=min_py) && y(1)>=max_py))
                    %                     if((y(end-1)>= max_p2y && y(2)<=min_p2y) || ((y(end-1)<=min_p2y) && y(2)>=max_p2y))
                    rectangle('Position',[min_x,min_y,(max_x-min_x),(max_y-min_y)],'EdgeColor','k','LineWidth',1.5);
                    Crackstruct(i).(next) = struct('CrackID',CrackNode(i).Crack.CrackID,'CrackLength',...,
                        max_y-min_y,'CrackWidth',CrackNode(i).Crack.CrackWidth,...,
                        'CrackArea',0,'Classif','Longitudinal','Severity',Crack_Severity,...,
                        'BoundingBox',[min_x,min_y;max_x,max_y]);
                else
                    min_x = min(CrackNode(i).Crack.value(:,1));
                    min_y = min(CrackNode(i).Crack.value(:,2));
                    max_x = max(CrackNode(i).Crack.value(:,1));
                    max_y = max(CrackNode(i).Crack.value(:,2));
                    %rectangle('Position',[min_x,min_y,(max_x-min_x),(max_y-min_y)],'EdgeColor','r','LineWidth',1.5);
                    Crack_area = ((max_x-min_x)/1000)*((max_y-min_y)/1000);
                    Crackstruct(i).(next) =  struct('CrackID',CrackNode(i).Crack.CrackID,'CrackLength',...,
                        CrackNode(i).Crack.CrackLength,...,
                        'CrackWidth',CrackNode(i).Crack.CrackWidth,...,
                        'CrackArea',Crack_area,'Classif','Fatigue','Severity',Crack_Severity,'BoundingBox',[min_x,min_y;max_x,max_y]);
                end
            end
        else
            min_x = min(CrackNode(i).Crack.value(:,1));
            min_y = min(CrackNode(i).Crack.value(:,2));
            max_x = max(CrackNode(i).Crack.value(:,1));
            max_y = max(CrackNode(i).Crack.value(:,2));
            %rectangle('Position',[min_x,min_y,(max_x-min_x),(max_y-min_y)],'EdgeColor','r','LineWidth',1.5);
            Crack_area = ((max_x-min_x)/1000)*((max_y-min_y)/1000);
            Crackstruct(i).(next) =  struct('CrackID',CrackNode(i).Crack.CrackID,'CrackLength',...,
                CrackNode(i).Crack.CrackLength,...,
                'CrackWidth',CrackNode(i).Crack.CrackWidth,...,
                'CrackArea',Crack_area,'Classif','Fatigue','Severity',Crack_Severity,'BoundingBox',[min_x,min_y;max_x,max_y]);
            
        end
    end
end

end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%Add Fatigue Cracks together%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [CrackClass,Crackfilt] = AddFatigueCracks(Crack_filt,CrackClass_full)

while(1)
    len1 = length(CrackClass_full);
    next = 'Crack';
    CrackClass = struct([]);
    Crackfilt = struct([]);
    
    c = zeros(size(CrackClass_full));
    for i=1:length(CrackClass_full)
        if(strcmpi(CrackClass_full(i).Crack.Classif,'Fatigue'))
            c(i)=i;
        end
    end
    c=c(c~=0);
    len = length(c);
    
    for i=1:len
        j=1;
        if(~isempty(Crack_filt(c(i)).Crack))
            while(j<=len && j~=i)
                first_Crack = Crack_filt(c(i)).Crack.value;
                if (~isempty(Crack_filt(c(j)).Crack))
                    next_Crack = Crack_filt(c(j)).Crack.value;
                    dist = distCalc(first_Crack,next_Crack);
                    if (dist < 300)
                        mod_val = [first_Crack;next_Crack];
                        min_x = min(mod_val(:,1));
                        min_y = min(mod_val(:,2));
                        max_x = max(mod_val(:,1));
                        max_y = max(mod_val(:,2));
                        Crack_area = ((max_x-min_x)/1000)*((max_y-min_y)/1000);
                        %rectangle('Position',[min_x,min_y,(max_x-min_x),(max_y-min_y)],'EdgeColor','r','LineWidth',1.5);
                        
                        if (CrackClass_full(c(i)).Crack.CrackWidth > CrackClass_full(c(j)).Crack.CrackWidth)
                            Crack_width = CrackClass_full(c(i)).Crack.CrackWidth;
                        else
                            Crack_width = CrackClass_full(c(j)).Crack.CrackWidth;                            
                            %                             if (~(strcmpi(CrackClass_full(c(i)).Crack.Severity,CrackClass_full(c(j)).Crack.Severity)))
                            %                                 CrackClass_full(c(i)).Crack.Severity = CrackClass_full(c(j)).Crack.Severity;
                            %                             end
                        end
                        
                        if((Crack_filt(c(i)).Crack.count==0) || (Crack_filt(c(j)).Crack.count==0))
                            Crack_filt(c(i)).Crack.count =  Crack_filt(c(i)).Crack.count + Crack_filt(c(j)).Crack.count+1;
                        else
                            Crack_filt(c(i)).Crack.count =  Crack_filt(c(i)).Crack.count + Crack_filt(c(j)).Crack.count;
                        end
                       
                        Crack_length = CrackClass_full(c(i)).Crack.CrackLength + CrackClass_full(c(j)).Crack.CrackLength;
                        
                        CrackClass_full(c(i)).Crack.CrackWidth = Crack_width;
                        CrackClass_full(c(i)).Crack.CrackArea = Crack_area;
                        CrackClass_full(c(i)).Crack.CrackLength = Crack_length;
                        CrackClass_full(c(i)).Crack.BoundingBox = [min_x,min_y;max_x,max_y];
                        CrackClass_full(c(j)).Crack = [];
                        Crack_filt(c(i)).Crack.value = mod_val;
                        Crack_filt(c(i)).Crack.CrackLength = Crack_length;
                        Crack_filt(c(i)).Crack.CrackWidth = Crack_width;
                        Crack_filt(c(j)).Crack = [];
                    end
                end
                j=j+1;
            end
        end
    end
    
    CrackClass_full = [CrackClass_full.Crack];
    Crack_filt = [Crack_filt.Crack];
    
    %k=1;
    
    for i=1:length(CrackClass_full)
        CrackClass(i).(next) =  struct('CrackID',CrackClass_full(i).CrackID,'CrackLength',...,
            CrackClass_full(i).CrackLength,...,
            'CrackWidth',CrackClass_full(i).CrackWidth,...,
            'CrackArea',CrackClass_full(i).CrackArea,'Classif',CrackClass_full(i).Classif,'Severity',CrackClass_full(i).Severity,...,
            'BoundingBox',CrackClass_full(i).BoundingBox);
    end
    
    for i=1:length(Crack_filt)
        Crackfilt(i).(next) = struct('CrackID',Crack_filt(i).CrackID,...,
            'CrackLength',Crack_filt(i).CrackLength,...,
            'CrackWidth',Crack_filt(i).CrackWidth,...,
            'value',Crack_filt(i).value,'group',Crack_filt(i).group,...,
            'count',Crack_filt(i).count,'primarydist',Crack_filt(i).primarydist);
    end
    
    len2 = length(CrackClass_full);
    CrackClass_full = CrackClass;
    Crack_filt = Crackfilt;
    if ((len2-len1)==0)
        break;
    end
    
end

for i=1:length(CrackClass)
    
    if (strcmpi(CrackClass(i).Crack.Classif,'Fatigue')==1)
        countTotal = Crack_filt(i).Crack.count;
        crack_width = Crack_filt(i).Crack.CrackWidth;
        
        min_x = CrackClass(i).Crack.BoundingBox(1,1);
        min_y = CrackClass(i).Crack.BoundingBox(1,2);
        max_x = CrackClass(i).Crack.BoundingBox(2,1);
        max_y = CrackClass(i).Crack.BoundingBox(2,2); 
        
        if ((countTotal <=20) && (countTotal >= 0))
            CrackClass(i).Crack.Severity = 'Low';
            rectangle('Position',[min_x,min_y,(max_x-min_x),(max_y-min_y)],'EdgeColor','r','LineWidth',1);
        end
        
        if (((countTotal <=100) && (countTotal > 20)) && (crack_width <= 6))
            CrackClass(i).Crack.Severity = 'Low';
            rectangle('Position',[min_x,min_y,(max_x-min_x),(max_y-min_y)],'EdgeColor','r','LineWidth',1);
        end
        
        if ((countTotal <=50) && (countTotal > 20))
            CrackClass(i).Crack.Severity = 'Moderate';
            rectangle('Position',[min_x,min_y,(max_x-min_x),(max_y-min_y)],'EdgeColor','r','LineWidth',2);
        end
        
        if (((countTotal <=100) && (countTotal > 50)) && (crack_width > 6 && crack_width <= 19))
            CrackClass(i).Crack.Severity = 'Moderate';
            rectangle('Position',[min_x,min_y,(max_x-min_x),(max_y-min_y)],'EdgeColor','r','LineWidth',2);
        end
        
        %         if ((countTotal > 25) && (countTotal <= 100)) && (crack_width > 6 && crack_width <= 19)
        %             CrackClass(i).Crack.Severity = 'Moderate';
        %             rectangle('Position',[min_x,min_y,(max_x-min_x),(max_y-min_y)],'EdgeColor','r','LineWidth',2);
        %         end
        
        if (countTotal > 100) || (((countTotal > 50) && (countTotal <= 100)) && crack_width > 19)
            CrackClass(i).Crack.Severity = 'High';
            rectangle('Position',[min_x,min_y,(max_x-min_x),(max_y-min_y)],'EdgeColor','r','LineWidth',4);
        end
    end
end

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%Add Transverse Cracks%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [CrackClass,Crackfilt] = AddTransverseCracks(Crack_filt,CrackClass_full)

while(1)
    len1 = length(CrackClass_full);
    next = 'Crack';
    CrackClass = struct([]);
    Crackfilt = struct([]);
    
    c = zeros(size(CrackClass_full));
    for i=1:length(CrackClass_full)
        if(strcmpi(CrackClass_full(i).Crack.Classif,'Transverse'))
            c(i)=i;
        end
    end
    c=c(c~=0);
    len = length(c);
    
    for i=1:len
        if(~isempty(Crack_filt(c(i)).Crack))
            j=1;
            while(j<=len && j~=i)
                first_Crack = Crack_filt(c(i)).Crack.value;
                if (~isempty(Crack_filt(c(j)).Crack))
                    next_Crack = Crack_filt(c(j)).Crack.value;
                    dist = distCalc(first_Crack,next_Crack);
                    ind = ismember(first_Crack,next_Crack);
                    ind_y = ind(:,2);
                    ind_y = ind_y(ind_y~=0);
                    
                    if ((dist < 300) && ~(isempty(ind_y)))
                        mod_val = [first_Crack;next_Crack];
                        min_x = min(mod_val(:,1));
                        min_y = min(mod_val(:,2));
                        max_x = max(mod_val(:,1));
                        max_y = max(mod_val(:,2));
                        Crack_area = 0;
                        %Crack_area = ((max_x-min_x)/1000)*((max_y-min_y)/1000);
                        rectangle('Position',[min_x,min_y,(max_x-min_x),(max_y-min_y)],'EdgeColor','b','LineWidth',1.5);
                        if (CrackClass_full(c(i)).Crack.CrackWidth > CrackClass_full(c(j)).Crack.CrackWidth)
                            Crack_width = CrackClass_full(c(i)).Crack.CrackWidth;
                        else
                            Crack_width = CrackClass_full(c(j)).Crack.CrackWidth;
                            if (~(strcmpi(CrackClass_full(c(i)).Crack.Severity,CrackClass_full(c(j)).Crack.Severity)))
                                CrackClass_full(c(i)).Crack.Severity = CrackClass_full(c(j)).Crack.Severity;
                            end
                        end
                        
                        Crack_length = CrackClass_full(c(i)).Crack.CrackLength + CrackClass_full(c(j)).Crack.CrackLength;
                        
                        CrackClass_full(c(i)).Crack.CrackWidth = Crack_width;
                        CrackClass_full(c(i)).Crack.CrackArea = Crack_area;
                        CrackClass_full(c(i)).Crack.CrackLength = Crack_length;
                        CrackClass_full(c(i)).Crack.BoundingBox = [min_x,min_y;max_x,max_y];
                        CrackClass_full(c(j)).Crack = [];
                        Crack_filt(c(i)).Crack.value = mod_val;
                        Crack_filt(c(j)).Crack = [];
                    end
                end
                j=j+1;
            end
        end
    end
    
    CrackClass_full = [CrackClass_full.Crack];
    Crack_filt = [Crack_filt.Crack];
    
    
    for i=1:length(CrackClass_full)
        CrackClass(i).(next) =  struct('CrackID',CrackClass_full(i).CrackID,'CrackLength',...,
            CrackClass_full(i).CrackLength,...,
            'CrackWidth',CrackClass_full(i).CrackWidth,...,
            'CrackArea',CrackClass_full(i).CrackArea,'Classif',CrackClass_full(i).Classif,'Severity',CrackClass_full(i).Severity,...,
            'BoundingBox',CrackClass_full(i).BoundingBox);
        
    end
    
    for i=1:length(Crack_filt)
        Crackfilt(i).(next) = struct('CrackID',Crack_filt(i).CrackID,...,
            'CrackLength',Crack_filt(i).CrackLength,...,
            'CrackWidth',Crack_filt(i).CrackWidth,...,
            'value',Crack_filt(i).value,'group',Crack_filt(i).group);
    end
    
    len2 = length(CrackClass);
    CrackClass_full = CrackClass;
    Crack_filt = Crackfilt;
    if ((len2-len1)==0)
        break;
    end
    
end

end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%Longitudinal Cracking%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [CrackClass,Crackfilt] = AddLongitudinalCracks(Crack_filt,CrackClass_full)

while(1)
    len1 = length(CrackClass_full);
    next = 'Crack';
    CrackClass = struct([]);
    Crackfilt = struct([]);
    
    c = zeros(size(CrackClass_full));
    for i=1:length(CrackClass_full)
        if(strcmpi(CrackClass_full(i).Crack.Classif,'Longitudinal'))
            c(i)=i;
        end
    end
    c=c(c~=0);
    len = length(c);
    
    for i=1:len
        if(~isempty(Crack_filt(c(i)).Crack))
            j=1;
            while(j<=len && j~=i)
                first_Crack = Crack_filt(c(i)).Crack.value;
                if (~isempty(Crack_filt(c(j)).Crack))
                    next_Crack = Crack_filt(c(j)).Crack.value;
                    dist = distCalc(first_Crack,next_Crack);
                    if (dist < 300)
                        mod_val = [first_Crack;next_Crack];
                        min_x = min(mod_val(:,1));
                        min_y = min(mod_val(:,2));
                        max_x = max(mod_val(:,1));
                        max_y = max(mod_val(:,2));
                        Crack_area = 0;
                        %Crack_area = ((max_x-min_x)/1000)*((max_y-min_y)/1000);
                        rectangle('Position',[min_x,min_y,(max_x-min_x),(max_y-min_y)],'EdgeColor','k','LineWidth',1.5);
                        if (CrackClass_full(c(i)).Crack.CrackWidth > CrackClass_full(c(j)).Crack.CrackWidth)
                            Crack_width = CrackClass_full(c(i)).Crack.CrackWidth;
                        else
                            Crack_width = CrackClass_full(c(j)).Crack.CrackWidth;
                            if (~(strcmpi(CrackClass_full(c(i)).Crack.Severity,CrackClass_full(c(j)).Crack.Severity)))
                                CrackClass_full(c(i)).Crack.Severity = CrackClass_full(c(j)).Crack.Severity;
                            end
                        end
                        
                        Crack_length = CrackClass_full(c(i)).Crack.CrackLength + CrackClass_full(c(j)).Crack.CrackLength;
                        
                        CrackClass_full(c(i)).Crack.CrackWidth = Crack_width;
                        CrackClass_full(c(i)).Crack.CrackArea = Crack_area;
                        CrackClass_full(c(i)).Crack.CrackLength = Crack_length;
                        CrackClass_full(c(i)).Crack.BoundingBox = [min_x,min_y;max_x,max_y];
                        CrackClass_full(c(j)).Crack = [];
                        Crack_filt(c(i)).Crack.value = mod_val;
                        Crack_filt(c(j)).Crack = [];
                    end
                end
                j=j+1;
            end
        end
    end
    
    CrackClass_full = [CrackClass_full.Crack];
    Crack_filt = [Crack_filt.Crack];
    
    
    for i=1:length(CrackClass_full)
        CrackClass(i).(next) =  struct('CrackID',CrackClass_full(i).CrackID,'CrackLength',...,
            CrackClass_full(i).CrackLength,...,
            'CrackWidth',CrackClass_full(i).CrackWidth,...,
            'CrackArea',CrackClass_full(i).CrackArea,'Classif',CrackClass_full(i).Classif,'Severity',CrackClass_full(i).Severity,...,
            'BoundingBox',CrackClass_full(i).BoundingBox);
        
    end
    
    for i=1:length(Crack_filt)
        Crackfilt(i).(next) = struct('CrackID',Crack_filt(i).CrackID,...,
            'CrackLength',Crack_filt(i).CrackLength,...,
            'CrackWidth',Crack_filt(i).CrackWidth,...,
            'value',Crack_filt(i).value,'group',Crack_filt(i).group);
    end
    
    len2 = length(CrackClass);
    CrackClass_full = CrackClass;
    Crack_filt = Crackfilt;
    if ((len2-len1)==0)
        break;
    end
    
end

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%% Distance Calculation%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function min_dist = distCalc(L1,L2)
min_dist = 1000000;

len1 = length(L1(:,1));
len2 = length(L2(:,1));

if (len1 >len2)
    dist = zeros(len2,1);
    len = len1;
else
    dist = zeros(len1,1);
    len = len2;
end

for i=1:len
    for j=1:length(dist)
        if (len1>len2)
            dist(j) = sqrt((L1(i,1)-L2(j,1))^2+(L1(i,2)-L2(j,2))^2);
        else
            dist(j) = sqrt((L2(i,1)-L1(j,1))^2+(L2(i,2)-L1(j,2))^2);
        end
        if (dist(j) < min_dist)
            min_dist = dist(j);
        end
    end
end

end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%Joining Crack data according to%%%%%%%%%%%%%%%%%%%%%%%%%%%
function C = joinLineSegment(A,B)

if (size(B,1) > size(A,1))
    dir = 0;
else
    dir = 1;
end

if (dir==0)
    for i=1:size(A,1)
        [~,indx] = min(abs(B(:,1)-A(i,1)));
        B=[B(1:indx,:);A(i,:);B((indx+1:end),:)];
        %break;
    end
    C = B;
else
    for i=1:size(B,1)
        [~,indx] = min(abs(A(:,1)-B(i,1)));
        A=[A(1:indx,:);B(i,:);A((indx+1:end),:)];
        %break;
    end
    C =A;
end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%XML parsing%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [z,str] = parse_xml(str,current_tag, idx)

next = 'children';

if (nargin < 2)
    current_tag   = '';
    idx           = 0;
end
z = struct([]);
eot = 0;
%main loop%
while ~eot && ~isempty(deblank(str))
    f_end = strfind(str, '</');
    f_beg = strfind(str, '<');
    if isempty(f_end) && isempty(f_beg)
        return;
    end
    
    if isempty(f_end)
        f_end = length(str);
    else
        f_end = f_end(1);
    end
    if isempty(f_beg)
        f_beg = length(str);
    else
        f_beg = f_beg(1);
    end
    
    if f_end <= f_beg
        new_tag = str((f_end+2):end);
        str_t   = str(1:f_end-1);
        f_end = strfind(new_tag,'>');
        
        if (isempty(f_end))
            return;
        end
        
        f_end = f_end(1);
        str = new_tag(f_end+1:end); % reset
        new_tag = new_tag(1:f_end-1);
        if ~strcmpi(new_tag, current_tag)
            return;
        end
        
        z(1).tag     = current_tag;
        z(1).attribs = '';
        z(1).value   = char(str_t);
        eot       = 1;
        
    else
        %current_value = str(1:f_beg-1);
        new_tag   = str(f_beg+1:end);
        f_end = strfind(new_tag,'>');
        if isempty(f_end)
            return;
        end
        f_end   = f_end(1);
        str_t   = new_tag(f_end+1:end);
        new_tag = new_tag(1:f_end-1);
        if (new_tag(end) == '/')||(new_tag(end) == '?')
            eot = 1;
        end
        
        f_beg   = strfind(new_tag, ' ');
        
        if isempty(f_beg)
            if eot
                new_tag = new_tag(1:end-1);
            end
        else
            new_tag     = new_tag(1:f_beg-1);
        end
        
        if eot
            new_attribs = '';
            if isfield(z,next)
                nxt = z(1).(next);
                nxt(end+1)=struct( 'tag', new_tag, 'attribs',new_attribs, 'value', '', next, []);
                z(1).(next) = nxt;
            else
                z(1).(next) = struct( 'tag', new_tag, 'attribs', new_attribs, 'value', '', next, []);
            end
            str = str_t;
            eot = 0;
        else
            new_attribs = '';
            [t,str] = parse_xml(str_t, new_tag, 1+idx);
            if isfield(t, next)
                nx = t(1).(next);
            else
                nx = [];
            end
            
            if isfield(z, next)
                nxt = z(1).(next);
                nxt(end+1) = struct( 'tag', t.tag, 'attribs', new_attribs, 'value', t.value, next, nx);
                z(1).(next) = nxt;
            else
                z(1).(next) = struct( 'tag', t.tag, 'attribs', new_attribs, 'value', t.value, next, nx);
            end
        end
    end
end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%