function numRect = findRectangles(pointList)

pointList = sortrows(pointList,[1,2]);
%pointprobRightUp = [0,0];
numRect = 0;

for i=1:size(pointList,1)
    pointLeftDown = pointList(i,:);
    for j=i+1:size(pointList,1)
        pointLeftUp = pointList(j,:);
        if(pointLeftDown(:,1)==pointLeftUp(:,1))
            if(pointLeftDown(:,2)~=pointLeftUp(:,2))
                for k=j+1:size(pointList,1)
                    pointRightDown = pointList(k,:);
                    if (pointLeftDown(:,2)==pointRightDown(:,2))
                        pointprobRightUp = [pointRightDown(:,1),pointLeftUp(:,2)];
                        a = intersect(pointList, pointprobRightUp,'rows');
                        if (~isempty(a))
                            numRect = numRect+1;
                            reduc_points = [pointLeftUp;pointLeftDown;pointRightDown;pointprobRightUp];
                            rectangle('Position',[min(reduc_points(:,1)),min(reduc_points(:,2)),max(reduc_points(:,1))-min(reduc_points(:,1)),...,
                                max(reduc_points(:,2))-min(reduc_points(:,2))],'EdgeColor','r','LineWidth',1.5);
                        end
                    end
                end
            end
        end
    end
end

end