function numSquares = findSquare(points_List)

reduc_points = [];
numSquares = 0;

for i=1:length(points_List)
    for j=(i+1):length(points_List)
        diffRotated = [points_List(j,2)-points_List(i,2),points_List(i,1)-points_List(j,1)];
        if (diffRotated(1,1)== 0 && diffRotated(1,2)== 0)
            diffRotated = [];
        end
        if (~(isempty(diffRotated)))
            m = intersect(points_List,points_List(i,:)+diffRotated,'rows');
            %m=m(m~=0);
            n = intersect(points_List,points_List(j,:)+diffRotated,'rows');
            %n=n(n~=0);
            
            a = intersect(points_List,points_List(i,:)-diffRotated,'rows');
            %a = a(a~=0);
            b = intersect(points_List,points_List(j,:)-diffRotated,'rows');
            %b = b(b~=0);          
                        
            if (~isempty(m)) && (~isempty(n))
                if (isempty(reduc_points))
                    reduc_points = [points_List(i,:); points_List(j,:);points_List(i,:)+diffRotated; points_List(j,:)+diffRotated];
                    rectangle('Position',[min(reduc_points(:,1)),min(reduc_points(:,2)),max(reduc_points(:,1))-min(reduc_points(:,1)),...,
                               max(reduc_points(:,2))-min(reduc_points(:,2))],'EdgeColor','r','LineWidth',1.5);
                    numSquares = numSquares+1;
                else
                    reduc_points1 = [points_List(i,:); points_List(j,:);points_List(i,:)+diffRotated; points_List(j,:)+diffRotated];
                    if (sortrows(reduc_points,[1,2]) ~= sortrows(reduc_points1,[1,2]))
                        numSquares = numSquares+1;
                        rectangle('Position',[min(reduc_points1(:,1)),min(reduc_points1(:,2)),max(reduc_points1(:,1))-min(reduc_points1(:,1)),...,
                               max(reduc_points1(:,2))-min(reduc_points1(:,2))],'EdgeColor','r','LineWidth',1.5);
                    end
                end
            elseif (~isempty(a)) && (~isempty(b))
                if (isempty(reduc_points))
                    reduc_points = [points_List(i,:); points_List(j,:);points_List(i,:)-diffRotated; points_List(j,:)-diffRotated];
                    rectangle('Position',[min(reduc_points(:,1)),min(reduc_points(:,2)),max(reduc_points(:,1))-min(reduc_points(:,1)),...,
                               max(reduc_points(:,2))-min(reduc_points(:,2))],'EdgeColor','r','LineWidth',1.5);
                    numSquares = numSquares+1;
                else
                    reduc_points1 = [points_List(i,:); points_List(j,:);points_List(i,:)-diffRotated; points_List(j,:)-diffRotated];
                    if (sortrows(reduc_points,[1,2]) ~= sortrows(reduc_points1,[1,2]))
                        numSquares = numSquares+1;
                        rectangle('Position',[min(reduc_points1(:,1)),min(reduc_points1(:,2)),max(reduc_points1(:,1))-min(reduc_points1(:,1)),...,
                               max(reduc_points1(:,2))-min(reduc_points1(:,2))],'EdgeColor','r','LineWidth',1.5);
                    end
                end
            end
        end                       
    end
end


end